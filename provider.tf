# Azure Provider
provider "azurerm" {
  version = "=2.0.0"
  features {}
}

# State Backend
terraform {
  backend "azurerm" {
    resource_group_name   = "KZEU-BRSH-DEV-SB-RGP-02"
    storage_account_name  = "terraform2244 "
    container_name        = "terraform-state"
    key                   = "+5HXVoX2mqEcPSXZFA4caBELtwT6tDKI+cHYbDw2slp6PO8fcrSq/jgX6QVj6VVzOVhc150awVM1068ByU1PPQ=="
  }
}
