output "app_service_plan" {
  description = "app service plan created created for the function app"
  value       = azurerm_app_service_plan.serviceplan.id
  sensitive   = true
}
