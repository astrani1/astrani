resource "azurerm_app_service_plan" "serviceplan" {
  name                = var.aspname
  location            = var.location
  resource_group_name = var.rgname

  sku {
    tier = "Standard"
    size = "S1"
  }
}