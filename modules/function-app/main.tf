  
resource "azurerm_function_app" "fn" {
  name                      = var.fname
  location                  = var.location
  resource_group_name       = var.rgname
  app_service_plan_id       = var.app_service_plan_id
  storage_connection_string = var.storage_connection_string
  https_only                = true
  client_affinity_enabled   = false

  site_config {
    always_on = true
  }

  identity {
    type = "SystemAssigned"
  }

  app_settings = var.app_settings

  lifecycle {
    ignore_changes = [app_settings]
  }

}