variable "fname" {
    type = string
    description = "Name of function app"
}

variable "rgname" {
    type = string
    description = "Name of resource group"
}

variable "location" {
    type = string
    description = "Azure location of storage account environment"
}

variable "app_settings" {
  default     = {}
  type        = map(string)
  description = "Application settings to insert on creating the function app. Following updates will be ignored, and has to be set manually. Updates done on application deploy or in portal will not affect terraform state file."
}

variable "storage_connection_string" {
    type = string
    description = "Name of storage conection string"
}


variable "app_service_plan_id" {
    type = string
    description = "id of asp"
}