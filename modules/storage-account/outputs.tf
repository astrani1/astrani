output "primary_key" {
    description = "The primary access key for the storage account"
    value = azurerm_storage_account.sa.primary_access_key
    sensitive   = true
}

output "storage_account_connection_string" {
  description = "Connection string to the storage account created for the function app"
  value       = azurerm_storage_account.sa.primary_connection_string
  sensitive   = true
}