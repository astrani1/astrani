resource "azurerm_sql_server" "example" {
  name                         = var.server_name
  resource_group_name          = var.rgname
  location                     = var.location
  version                      = "12.0"
  administrator_login          = "4dm1n157r470r"
  administrator_login_password = "4-v3ry-53cr37-p455w0rd"
}

resource "azurerm_sql_database" "example" {
  name                = var.sqldbname
  resource_group_name = var.rgname
  location            = var.location
  server_name         = var.server_name

  tags = {
    environment = "dev"
  }
}