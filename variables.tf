variable "location" {
    type = string
    description = "Azure location of storage account environment"
}

variable "sa" {
    type = string
    description = "Azure location of storage account environment"
}

variable "aspname0" {
    type = string
    description = "Name of resource group"
}

variable "fname" {
    type = string
    description = "Name of function app"
}

variable "account_replication_type" {
  default     = "LRS"
  description = "The Storage Account replication type. See azurerm_storage_account module for posible values."
}

variable "sqldbname" {
    type = string
    description = "Name of function app"
}


variable "azurerm_sql_server" {
    type = string
    description = "Name of function app"
}
