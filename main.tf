
#create resource group
module "azurerm_resource_group" {
    source = "./modules/resource-group"
    rgname     = "kzeu-brsh-sb-dev-rgp-05"
    location = var.location
}

#Create Storage Account
module "storage_account" {
  source    = "./modules/storage-account"
  saname    = var.sa
  rgname    = "kzeu-brsh-sb-dev-rgp-05"
  location  = var.location
}


#Create SqlDatabase Account
module "sql-database" {
  source    = "./modules/sql-database"
  server_name = var.azurerm_sql_server
  sqldbname =  var.sqldbname
  rgname    = "kzeu-brsh-sb-dev-rgp-05"
  location  = var.location
}

